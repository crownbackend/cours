<?php 

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends Controller {
    
    /**
     * @Route("/admin/index")
     * @return Response
     */
    public function index(): Response {

        return new Response('<html><body><h1>zone admin</h1></body></html>');
    }

    /**
     * @Route("/admin/add-role")
     * @return Response
     */
    public function addRole(Request $request): Response {
        // On récupère les variables POSTS
        $userChoosed = (int) $request->request->get('chosed-user');
        $roleChoosed = $request->request->get('chosed-role');
        // On récupère le Repository (pour obtenir l'utilisateur voulu et ensuite tous les utilisateurs)
        $repo = $this->getDoctrine()->getRepository(User::class);
        // On verifie que les variables POSTS ne sont pas nulles pour modifier en BDD
        if (!is_null($userChoosed) && !is_null($roleChoosed) ) {
            // On récupère l'utilisateur correspondant a l'id passé 
            $user = $repo->find($userChoosed);
            
            if(!$user) {
                throw new \Exception('Utilisateur inconnu');
            }
            // On ajoute le role a l'utilisateur
            $user->setRole($roleChoosed);
            // on enregistre l'utilisateur en BDD
            $manager = $this->getDoctrine()->getManager();
            $manager->flush();
        }

        $users = $this->getDoctrine()
                      ->getRepository(User::class)
                      ->findAll();

        return $this->render('admin/add_role.html.twig', [
            'users' => $users,
            'title' => 'Role des utilisateurs'
        ]);
    }
}