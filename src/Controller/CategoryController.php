<?php 

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;



class CategoryController extends Controller {


    /**
     * @Route("/categories/ajout")
     * @return Response
     */
    public function add(Request $request): Response {

        $categorie = new Category();

        $form = $this->createForm(CategoryType::class, $categorie);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {


            $categorie = $form->getData();
            $category = $this->getDoctrine()->getManager();
            $category->persist($categorie);
            $category->flush();

            $this->addFlash('notice', 'La categorie a bien été ajouté');
            return $this->redirectToRoute('app_product_index');
        }

        return $this->render('category/add.html.twig', [
            'title' => 'Ajouter un produit a la liste',
            'form' => $form->createView()
        ]);
    }







}