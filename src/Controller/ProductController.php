<?php 

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class ProductController extends Controller {

    /**
     * @Route("/index")
     * @return Response
     */
    public function index(): Response {
        

        return $this->render('index.html.twig', [
            'active' => 'active',
            'title' => 'Bienvenue sur ma boutique'
        ]);

    }

    /**
     * @Route("/")
     * @return Response
     */
    public function index3(): Response {
        

        return $this->render('index.html.twig', [
            'active' => 'active',
            'title' => 'Bienvenue sur ma boutique'
        ]);

    }

    // route en Yaml
    public function index2() {
        
        return $this->render('index.html.twig', [
            'active' => 'active',
            'title' => 'Bienvenue sur ma boutique'
        ]);
    }

    /**
     * @Route("/produits")
     * @return Response
     */

     public function produits(Request $request): Response {


         $em    = $this->get('doctrine.orm.entity_manager');
         $dql   = "SELECT p FROM App:Product p";
         $query = $em->createQuery($dql);

         $paginator  = $this->get('knp_paginator');
         $pagination = $paginator->paginate(
             $query,
             $request->query->getInt('page', 1)/*page number*/,
             9/*limit per page*/
         );

        // Récupération des produits
        // On retourne la vue en passant les produits
       // $products = $this->getDoctrine()
        //->getRepository(Product::class)
        //->findBy([], ['createdAt' => 'DESC'])

        // on retourne la vue avec tout les produits
        return $this->render('product/list.html.twig', [
            'title' => 'Liste de tout les produits',
            'pagination' => $pagination
        ]);
     }


     /**
     * @Route("/produits/gestion/ajout")
     * @return Response
     */
    public function add(Request $request): Response {

        $product = new Product();

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {


            $product = $form->getData();
            $products = $this->getDoctrine()->getManager();
            $products->persist($product);
            $products->flush();

            $this->addFlash('notice', 'Le produit a bien été ajouté');
            return $this->redirectToRoute('app_product_index');
        }

        return $this->render('product/add.html.twig', [
            'title' => 'Ajouter un produit a la liste',
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/produits/{id}")
     * @param int $id
     * @return Response
     */
    public function show(int $id): Response {

        $product = $this->getDoctrine()
                        ->getRepository(Product::class)
                        ->findOneWithCategory($id);

        return $this->render('product/show.html.twig', [
            'product' => $product,
            'title' => $id
        ]);
     }

     /**
     * @Route("/produits/gestion/edit/{id}")
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function edit(Request $request, int $id): Response {

        $product = $this->getDoctrine()
                        ->getRepository(Product::class)
                        ->find($id);

        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {


            $product = $form->getData();

            $products = $this->getDoctrine()->getManager();
            $products->flush();

            return $this->redirectToRoute('app_product_produits');
        }

        return $this->render('product/edit.html.twig', [
            'title' => 'Ajouter un produit a la liste',
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/produits/gestion/delete/{id}")
     * @param int $id
     * @return Response
     */
    public function delete(int $id): Response {

        $product = $this->getDoctrine()
                        ->getRepository(Product::class)
                        ->find($id);

        $manager = $this->getDoctrine()->getManager();
        $manager->remove($product);
        $manager->flush();

        return $this->redirectToRoute('app_product_produits');
     }


}