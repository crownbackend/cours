<?php

namespace App\Admin;

use App\Entity\Category;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ProductAdmin extends AbstractAdmin {

    protected function configureListFields(ListMapper $list) {
        $list
            ->addIdentifier('name')
            ->add('price')
            ->add('nbViews')
            ->add('isPublished')
            ->add('category.name')
            ->add('imageFile', FileType::class)
            ->add('_action', 'actions', array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
            ;
    }

    protected function configureFormFields(FormMapper $form) {
        $form
            ->add('name')
            ->add('description')
            ->add('price')
            ->add('imageFile', FileType::class)
            ->add('isPublished')
            ->add('category', ModelType::class, [
                'class' => Category::class,
                'property' => 'name',
            ])
            ;
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('price')
            ->add('createdAt')
            ->add('category')
            ;
    }
    protected function configureShowFields(ShowMapper $show)
    {
        $show
            ->add('name', Product::class)
            ->add('price')
            ->add('imageFile', FileType::class)
            ->add('nbViews')
            ->add('isPublished')
            ->add('category.name')
            ;
    }
}