<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\IdentityUser", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $identity;

    public function getId()
    {
        return $this->id;
    }

    public function getIdentity(): ?IdentityUser
    {
        return $this->identity;
    }

    public function setIdentity(IdentityUser $identity): self
    {
        $this->identity = $identity;

        return $this;
    }

    /**
     * Place un role unique a l'utilisateur (supprime tous les anciens roles)
     * @param string $userRole
     */
    public function setRole(string $userRole) {

        // Vider les roles
        foreach ($this->getRoles() as $role) {

            $this->removeRole($role);
        }
        // ajout le role unique passé en paramètre
        $this->addRole($userRole);
    }
}
