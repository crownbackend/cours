<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\ORM\Doctrine\Populator;

class AppFixtures extends Fixture {


    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) {


        // création d'une catégorie
         /*  $category = new Category();
           $category->setName('Art et nature');

           $manager->persist($category);

        // création du produit
        for ($i = 0; $i < 9; $i++) {
            $product = new Product();
            $product->setName('product '.$i);
            $product->setDescription("azrraz");
            $product->setIsPublished(true);
            $product->setImageName('azrza.png');
            $product->setImageSize(98798);
            $product->setCategory($category);
            $product->setPrice(mt_rand(10, 100));
            $manager->persist($product);
        }
        // enregistrement du produit
        $manager->flush();*/

        $generator = \Faker\Factory::create();
        $populator = new Populator($generator, $manager);

        $populator->addEntity(Category::class, 10);
        $populator->addEntity(Product::class, 200, [
            'price' => function() use($generator) {
            return $generator->randomFloat(2,0,999.99);
        },
            'imageName' => function() { return 'test.jpg'; }
        ]);

        $populator->execute();
    }
}